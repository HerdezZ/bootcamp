# Set the base image to get the secret from Vault
FROM python:3.7-alpine


# Add pipfiles
COPY Pipfile /Pipfile
COPY Pipfile.lock /Pipfile.lock

# Install minimal software for production
RUN set -ex \
    && apk update \
    && pip3 install pipenv \
    && pipenv install --deploy --system --ignore-pipfile \
    && rm -rf /var/cache/apk/*

# Add code and script from proyect
COPY . /srv
WORKDIR /srv

ENV FLASK_APP=ping.py

ENTRYPOINT [ "flask", "run", "-h",  "0.0.0.0" ]
